<?php
namespace App\Controllers;

use App\Controllers\Base\BaseController;
use App\Models\User;



class WelcomeController extends BaseController
{
	
	public function __construct()
	{
		parent::__construct();

	}


	/**
	 * @param null $args
	 * @return string
     */
	public function index($args=null)
	{

		$user = new User();
		$q_users = $user->obtener_usuarios();
		

		return $this->_view('welcome', 
			array('pTitle' => 'Lemon Framework',
				  'pContent' => 'Bienvenido a Lemon!',
				  'author' => '@ferdroid8080',
				  'users' => $q_users));


	}



}