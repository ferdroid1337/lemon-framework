<?php
require ROOT . DS . 'vendor' . DS . 'autoload.php';

use Dotenv\Dotenv;
use Lemon\Core;

// Loading environment variables
$dotenv = new Dotenv(__DIR__ . '/../');
$dotenv->load();


// set environment app
$system = new Core\System();
$system->set_environment_app();

// disable magic quotes
$system->remove_magic_quotes();

// check if globals are enabled to disable it
$system->unregister_globals();


require ROOT . DS . 'config' . DS . 'db.php';

/** Calling main function of the app **/
$system->configure();

