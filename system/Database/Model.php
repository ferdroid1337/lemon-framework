<?php
namespace Lemon\Database;


class Model
{
    protected $_database;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $this->_database = new DB_Tools();
    }


    /**
     * Reset method allows clean an re-create the DB_Tools Object
     * in order to do any operation else
     */
    public function reset()
    {
        $this->_database = null;
        $this->_database = new DB_Tools();
    }
    

}