<?php
namespace Lemon\Database;

use InvalidArgumentException;
use PDO;
use RuntimeException;


class DB_Tools extends DB_Connector implements DB_InterfaceAdapter
{
    
    protected $_statement;


    /**
     * DB_Tools constructor.
     */
    public function __construct()
    {
        parent::__construct();

    }


    /**
     * @param $query
     * @param null $conditions
     * @return $this|string
     */
    public function execute($query, $conditions=null)
    {
        if (!is_string($query) || empty($query)) {
            return 'The specified query is not valid.';
        }

        $this->_statement = $this->db->prepare($query) or die("Error executing the specified query: <br />" . $this->db->errorInfo()[2]);
        $this->_statement->execute($conditions);
        
        return $this;
    }


    /**
     * $where params must be an array of array of three elements where odd indexes
     * would be the logical operator and even indexes would be the three elements' array
     *
     * @param $table
     * @param array $fields
     * @param array $where
     * @param string $order
     * @param null $limit
     * @param null $offset
     * @return DB_Tools|string
     */
    public function select($table, $fields=[], $where=[], $order='', $limit=null, $offset=null)
    {
        if (empty($fields)) $fields = '*';
        else $fields = implode(', ', $fields);
        
        $wh = "";
        $wh_params = array();
        if (!empty($where)) {
            if (count($where) > 0) {

                foreach ($where as $i) {
                    if (is_array($i)) {
                        $wh_params[] = $i[2];
                        $wh .= $table . "." . $i[0] . " " . $i[1] . " ? ";
                    } else {
                        $wh .= $i . ' ';
                    }
                }
                
            } else {
                $wh_params[] = $where[2];
                $wh .= $table . "." . $where[0] . " " . $where[1] . " ? ";
            }
        }

        $prepareQuery = "SELECT " . $fields . " FROM " . $table . " " .( $wh!='' ? "WHERE " . $wh : "" ) .
                        ( $order!=null ? "ORDER by " . $order[0] . " " . $order[1] . " " : '' ) . ( $limit!=null ? "LIMIT " . $limit . 
                        ( $offset!=null ? ", " . $offset : '' ) : '' );
        
        //ds($prepareQuery);
        
        return $this->execute($prepareQuery, $wh_params);
    }


    /**
     * @param $table
     * @param array $data
     * @return null
     */
    public function insert($table, $data=array())
    {
        if (!empty($data)) {
            $fields = implode(', ', array_keys($data));
            
            $str_fields = "";
            foreach ($data as $k => $v) {
                $str_fields .= ":" . $k . ", ";
            }
            $str_fields = substr($str_fields, 0, -2);
            
            $prepareInsert = "INSERT INTO " . $table . " (".$fields.") VALUES (" . $str_fields .")";

            $params = array();
            foreach ($data as $k => $v) {
                $params[":" . $k] = $v;
            }

        } else return null;

        return $this->execute($prepareInsert, $params)->count_rows() > 0 ? $this->get_last_insert_id() : null; // zero for succeding in the insert query

    }


    /**
     * @param $table
     * @param array $data
     * @param array $conditions
     * @return int|null
     */
    function update($table, $data = array(), $conditions = [])
    {
        if (!empty($data)) {

            $set = array();
            $str_set = "";
            foreach ($data as $k => $v) {
                $set[":" . $k] = $v;
                $str_set .= "" . $k . "=:" . $k . ", ";
            }
            $str_set = substr($str_set, 0, -2);
            //show_output($set);

            $wh = "";
            $params = array();
            foreach ($conditions as $k => $v) {
                $params[":" . $k] = $v;
                $wh .= $k . "=:" . $k;
            }
            
            $params = array_merge($set, $params);
            //show_output($params);

            $updateQuery = "UPDATE " . $table . " SET $str_set WHERE $wh";
            
            //show_output($updateQuery);

        } else return null;

        return $this->execute($updateQuery, $params)->count_rows() > 0 ? $this->count_rows() : 0;

    }


    /**
     * @param $table
     * @param array $conditions
     * @return int|null
     */
    function delete($table, $conditions = [])
    {
        if (!empty($conditions) && count($conditions) < 2) {

            $wh = "";
            $params = array();

            foreach ($conditions as $k => $v) {
                $params[":" . $k] = $v;
                $wh .= $k . " = :" . $k;
            }
            
            $deleteQuery = "DELETE FROM " . $table ." WHERE " . $table . "." . $wh;

        } else return null;

        return $this->execute($deleteQuery, $params)->count_rows() > 0 ? $this->count_rows() : 0;
    }


    /**
     * @return null
     */
    private function get_last_insert_id()
    {
        return $this->db != null ? $this->db->lastInsertId() : null;
    }


    /**
     * @return int
     */
    private function count_rows()
    {
        return $this->_statement != null ? $this->_statement->rowCount() : 0;
    }

    /**
     * @return null
     */
    public function fetch() {
        
        if ($this->count_rows() > 0) {
            $rows = $this->_statement->fetchAll(PDO::FETCH_ASSOC);    
        } else {
            $rows = null;
        }
        $this->free_result();

        return $rows;
    }


    
    private function free_result()
    {
        $this->_statement->closeCursor();
        $this->_statement = null;
    }
    
}